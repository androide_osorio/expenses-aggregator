import io, mailbox, re, chardet

from argparse import ArgumentParser
from bs4 import BeautifulSoup
from mailbox import mbox
from os import path
from tabulate import tabulate

"""
This Module defines helper classes for reading and extracting
text from email messages stored as .mbox files. It can handle
simple and multipart messages and HTML messages.

It also provides utilities for extracting text out of HTML
messages.
"""
# -------------------------------------------------------------
# Message Parsing Methods

class MessageParser(object):
  @staticmethod
  def parse_multipart(message):
    """
    Generator for parsing the content of a multipart email message

    Arguments:
    message -- an mbox.Message object

    Returns:
    all of the message parts as an iterator
    """
    if message.is_multipart():
      for part in message.get_payload():
        yield from MessageParser.parse_multipart(part)
    else:
      yield message.get_payload(decode=True)

  @staticmethod
  def parse_content_type(message):
    """Extract the main content type of a multipart email message.
    When a message is a multipart one, it's primary content type is multipart/mixed.
    Each of the subparts of the message has its own main content type. This generator
    extracts the content type of the innermost message part and returns it.

    Arguments:
    message -- an mbox.Message object

    Returns:
    The message's true content type
    """
    if message.is_multipart():
      for part in message.get_payload():
        yield from MessageParser.parse_content_type(part)
    else:
      yield message.get_content_type()

  @staticmethod
  def extract_content(message):
    """Retrieve the content of an email message. If the message has multiple parts,
    it recursively retrieves all parts as a single message

    Arguments:
    message -- an mbox.Message object

    Returns:
    The message content as bytes
    """
    if message.is_multipart():
      parts = MessageParser.parse_multipart(message)
      return b''.join(part for part in parts)

    return message.get_payload(decode=True)

  @staticmethod
  def decode_content(message, content):
    """Decode the email message content using the appropriate charset.
    If the charset found in the message does not help in decoding the
    message content, the charset is then detected from the content and
    used to decode it.

    Arguments:
    message -- an mbox.Message object
    content -- the byte representation of the message content

    Returns:
    The decoded content as a string
    """
    charset = message.get_content_charset('utf-8')
    try:
      return content.decode(charset)
    except UnicodeDecodeError:
      detected_charset = chardet.detect(content)['encoding']
      return content.decode(detected_charset)

  @staticmethod
  def parse_by_content_type(message, content):
    """Extract the text of a multipart message, by its content type

    Arguments:
    message -- an mbox.Message object
    content -- the byte representation of the message content

    Returns:
    The parsed message content by its corresponding content type
    """
    content_type    = next(MessageParser.parse_content_type(message))
    decoded_content = MessageParser.decode_content(message, content)

    if 'plain' in content_type:
      return decoded_content
    if 'html' in content_type:
      return BeautifulSoup(decoded_content, 'html.parser')

  @staticmethod
  def sanitize(message_content):
    """Remove especial characters from an email message content
    and clean up repeated new lines and white spaces.

    Arguments:
    message_content -- message content as a string

    Return:
    Sanitized email content as a string
    """
    message_striped = re.sub(r'\n+', '\n', message_content)
    message_whitespaces = re.sub(r'\s{2,}', ' ', message_striped)
    return message_whitespaces.strip()

  @staticmethod
  def extract_text(message):
    """Extract all the text from an email message, stripping any special characters
    or html tags present in the content.

    Arguments:
    message -- message content as a string

    Return:
    Sanitized email content as a string, with no HTML tags or special characters
    """
    message_content = MessageParser.extract_content(message)
    decoded_message = MessageParser.parse_by_content_type(message, message_content)

    if isinstance(decoded_message, BeautifulSoup):
      return MessageParser.sanitize(decoded_message.get_text())
    else:
      return MessageParser.sanitize(decoded_message)

# -------------------------------------------------------------
class MailboxReader(object):
  mbox_file_path = path.dirname(path.realpath(__file__))

  def __init__(self, file_path):
    self.mbox_file_path = file_path

  def read(self):
    """Read the contents of an .mbox file and return each message as a dictionary.

    Arguments:
    mbox_path -- the local, absolute path to a .mbox file

    Returns:
    mailbox.mbox object
    """
    mbox_folder_path = path.join(self.mbox_file_path, "mbox")
    return mbox(mbox_folder_path, factory=None, create=False).values()

# -------------------------------------------------------------
# Main executable helper methods

def extract_fields(messages, fields, truncate):
  message_list = []

  for message in messages:
    message_content = MessageParser.extract_text(message)
    message_fields = [message[field] for field in message if field in fields]
    message_fields.append(message_content[:truncate] + '...')
    message_list.append(message_fields)

  return message_list

def get_summary(messages):
  messages_count = len(messages)
  message_senders = set(map(lambda m: m["From"], messages))
  oldest_message_date = messages[-1]["Date"]
  newest_message_date = messages[0]["Date"]

  data = [
    ["Total Messages", messages_count],
    ["Senders List", '\n'.join(message_senders)],
    ["Oldest Message sent in", oldest_message_date],
    ["Newest Message sent in", newest_message_date]
  ]
  headers = ["Item", "value"]
  return tabulate(data, headers, tablefmt="fancy_grid")

def create_argparser():
  parser = ArgumentParser(description="Parse the contents of a .mbox file")
  parser.add_argument('mbox_path', help='path to the .mbox file to be parsed')
  parser.add_argument('-f', '--fields', nargs='?', help="email fields to include in the result (only for verbose mode).", const=",".join(["Date", "From", "To", "Subject"]))
  parser.add_argument('-v', '--verbose', action='store_true', help='output process and results of the mailbox parsing')
  parser.add_argument('-n', '--print_emails', type=int, help='number of emails to print (only available in verbose mode)')
  parser.add_argument('-t', '--truncate', nargs='?', type=int, const=256, help='truncate the message content to the number of specified charactera(only available in verbose mode)')

  return parser

# -------------------------------------------------------------
if __name__ == "__main__":
  parser = create_argparser()
  args = parser.parse_args()

  print()
  print('=' * 80)
  print('Reading emails from file: {0}'.format(path.basename(args.mbox_path)))
  print(f'Full path: {args.mbox_path}')

  print()
  print('This may take a While...')
  print()

  # extract messages
  reader = MailboxReader(args.mbox_path)
  messages = reader.read()

  if args.verbose:
    messages_count = args.print_emails
    fields = args.fields.split(',')
    table_headers = fields + ["Content (truncated)"]
    data = extract_fields(messages[0:messages_count], fields, args.truncate)
    table = tabulate(data, headers=table_headers, tablefmt='pipe', showindex=True)

    print(table)

  # print email summary
  summary = get_summary(messages)
  summary_length = len(summary.split('\n')[0])
  print('=' * summary_length)
  print("| Summary")
  print('=' * summary_length)
  print(summary)
  print('DONE!')
