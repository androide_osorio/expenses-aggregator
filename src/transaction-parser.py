import re

# example
#  Bancolombia le informa Transferencia por $31,000 desde cta *9340 a cta 03151241785. 27/04/2018 14:54. Inquietudes al 0345109095/018000931987.

class TransactionParser:
  transaction_start = r'Bancolombia le informa (?P<action>.*)'
  transaction_amount = r'(?:por|a) \$(?P<amount>[\d,\.]+)'
  transaction_recipient = r'(?:en|a|desde) (?P<recipient>.*)'
  transaction_date = r'(?P<date>\d{2}/\d{2}/\d{4})'
  transaction_time = r'(?P<time>\d{,2}:\d{,2})'
# for index, message in messages[0:10]:
#   header_pattern = r'Bancolombia le informa (?P<action>.*)'
#   transaction_pattern = r'(?:por|a) \$(?P<amount>[\d,\.]+)'
#   metadata_pattern = r'(en|desde) (?P<recipient>.*) (?P<time>\d{,2}:\d{,2}). (?P<date>\d{2}/\d{2}/\d{4})'
#   complete_pattern = [header_pattern, transaction_pattern, metadata_pattern]
#   regexp = re.compile(' '.join(complete_pattern), re.MULTILINE)

#   cleaned_msg = message.as_string().replace('=', '').replace('\n', '').strip()

#   sanitized_msg = re.sub('\s{2,}', '', cleaned_msg)
#   matches = regexp.search(sanitized_msg)
#   if matches:
#     print(matches.groupdict())